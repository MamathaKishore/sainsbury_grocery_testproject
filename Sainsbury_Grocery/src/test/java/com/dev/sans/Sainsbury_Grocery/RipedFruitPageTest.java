package com.dev.sans.Sainsbury_Grocery;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.junit.Test;

public class RipedFruitPageTest {
	private static final double DELTA = 1e-15;

	@Test
	public void testGetTotalPrice() {

		RipedFruitPage obj = new RipedFruitPage();
		ArrayList<String> strPriceList = new ArrayList<String>();

		assertEquals(0.0, obj.getTotalPrice(strPriceList), DELTA);

	}

	@Test(expected = MalformedURLException.class)
	public void testURLConnection() throws MalformedURLException {
		String url = null;
		URL connectLink = new URL(url);

	}

}
