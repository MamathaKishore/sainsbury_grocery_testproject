package com.dev.sans.Sainsbury_Grocery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * console application that scrapes the Sainsbury’s grocery site - Ripe Fruits
 * page
 *
 * @author Mamatha.BV
 */
public class RipedFruitPage {
	public static void main(String[] args) {

		RipedFruitPage ripe = new RipedFruitPage();
		ripe.getGroceryList("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html");

	}

	// purpose of this method to retrieve Title,Unit price , size and
	// description values from Sainsbury Grocery url.
	public void getGroceryList(String url) {
		String line;
		String totalPrice = null;
		JSONObject jGroup = null;
		ArrayList<String> arrayPrices = new ArrayList<String>();
		JSONArray jArray = new JSONArray();
		JSONObject jResult = new JSONObject();
		JSONObject jGroup1 = null;
		try {
			URL htmlLink1 = new URL(url);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					htmlLink1.openStream()));

			while ((line = in.readLine()) != null) {

				Document doc = Jsoup.parse(line);
				Elements links = doc.select("a[href*=2015_Developer_Scrape]");

				for (Element e : links) {
					String htmlLink2 = e.attr("href");

					Document eventInfo = Jsoup.connect(htmlLink2).get();
					String strTitle = eventInfo.title();
					String substr = strTitle
							.substring(0, strTitle.indexOf("|"));

					Element price = eventInfo.select("p[class*=pricePerUnit]")
							.first();
					Element desc = eventInfo.select("div[class=productText]")
							.first();
					double size = eventInfo.outerHtml().length() * 0.001;

					arrayPrices.add(price.text());

					jGroup = new JSONObject();
					jGroup = createJson(substr, size, price.text(), desc.text());

					jArray.put(jGroup);

					// jResult.put("results", jArray);
				}
			}
			double totP = getTotalPrice(arrayPrices);

			jGroup1 = new JSONObject();
			jGroup1.put("totals", totP);
			jArray.put(jGroup1);
			jResult.put("results", jArray);

		} catch (MalformedURLException exception) {
			exception.printStackTrace();
		} catch (IOException exception) {
			exception.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(jResult);
	}

	// Purpose of this method to retrieve total Unitprice.
	public double getTotalPrice(ArrayList<String> price) {
		// TODO Auto-generated method stub
		double total = 0.0;
		for (int i = 0; i < price.size(); i++) {
			String uPrice = price.get(i);
			String totalPrice = uPrice.substring(1, uPrice.indexOf("/"));
			total = total + Double.parseDouble(totalPrice);
		}
		// String totalPrice = String.format("%.02f",total);
		// System.out.println("............."+totalPrice);
		DecimalFormat df2 = new DecimalFormat("##.##");
		return Double.valueOf(df2.format(total));
		// return total;
	}

	// purpose of this method to generate JSON object.
	public JSONObject createJson(String titlte, double size, String unit_price,
			String desc) {
		JSONObject objGrocery = new JSONObject();
		try {
			objGrocery.put("title", titlte);
			objGrocery.put("size", size + "kb");
			objGrocery.put("unit_price", unit_price);
			objGrocery.put("description", desc);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objGrocery;
	}

}
